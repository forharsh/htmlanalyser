<!DOCTYPE HTML>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Html Analyser Utility!</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link href="http://cdn.jsdelivr.net/webjars/bootstrap/3.3.4/css/bootstrap.min.css"
	rel="stylesheet" media="screen" />

<script src="http://cdn.jsdelivr.net/webjars/jquery/2.1.4/jquery.min.js"></script>

</head>
<body>

	<div class="container">
		<h2>Html Analyser</h2>
		<c:if test="${errorMsg.detail != ''}">
			<div class="alert alert-danger">
				<strong>Danger!</strong> <span><c:out value="${errorMsg.detail}" /></span>
			</div>
		</c:if>
		<form class="form-horizontal" action="/parser" method="post">
					<div class="form-group">
						<label class="control-label col-sm-1">URL:</label>
						<div class="col-sm-10 ">
							<input type="text" value="<c:out value="${htmlDocument.uri }"/>"
								name="uri" class="form-control"
								placeholder="Enter a URL to run analysis" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" value="Submit" class="btn btn-primary" /> <input
								type="reset" value="Reset" class="btn btn-danger" />
						</div>
					</div>
				</form>
	</div>
	<c:if test="${htmlDocument != null}">
		<div class="container">
				<div>
					<h3>Analysis Result</h3>
					<div class="panel panel-default">
						<div class="table-responsive">
							<table
								class="table table-striped table-hover table-condensed table-bordered">
								<thead>
									<tr>
										<th class="tableCentered">Html Version</th>
										<th class="tableCentered">Title</th>
										<th class="tableCentered">Headings</th>
										<th class="tableCentered">Internal Links</th>
										<th class="tableCentered">External Links</th>
										<th class="tableCentered">Login Form</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><c:out value="${htmlDocument.htmlVersion}" /></td>
										<td><c:out value="${htmlDocument.pageTitle}" /></td>
										<td><c:out value="${htmlDocument.headingTagMap}" /></td>
										<td><c:out value="${htmlDocument.internalLinksCount}" /></td>
										<td><c:out value="${htmlDocument.externalLinksCount}" /></td>
										<td><c:out value="${htmlDocument.hasLoginForm}" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
	
					<div class="panel panel-default">
						<div class="table-responsive">
							<table
								class="table table-striped table-hover table-condensed table-bordered">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Internal/External Links</th>
										<th scope="col">HTTP Response Code</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach varStatus="status" var="type"
										items="${htmlDocument.linkResourceValidationMap}">
										<tr>
											<td class="col-md-1" scope="row"><c:out value="${ status.count}" /></td>
											<td class="col-md-2"><c:out value="${type.key }" /></td>
											<td class="col-md-1"><c:out value="${type.value}" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
		</div>
</c:if>

</body>
</html>