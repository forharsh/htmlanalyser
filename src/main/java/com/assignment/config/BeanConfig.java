package com.assignment.config;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
public class BeanConfig {
	
	 private static final String[] SCHEMES = {"http","https"};

    @Bean
    public InternalResourceViewResolver setupViewResolver()  {
        final InternalResourceViewResolver resolver =  new InternalResourceViewResolver();
        resolver.setPrefix ("/WEB-INF/jsp/");
        resolver.setSuffix (".jsp");
        resolver.setViewClass (JstlView.class);
        return resolver;
    }
    
    @Bean
    public UrlValidator urlValidator() {
        return new UrlValidator(SCHEMES);
    }
}
