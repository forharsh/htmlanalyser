package com.assignment.thread;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidateLinkedResource implements Callable<Map<String, Integer>> {
	
	private static final Logger logger = LoggerFactory.getLogger(ValidateLinkedResource.class);

    private final List<String> resourceList;
    private final Map<String, Integer> resourceValidationMap = new HashMap<>();

    public ValidateLinkedResource(final List<String> resourceList) {
        this.resourceList = resourceList;
    }

    @Override
    public Map<String, Integer> call() {
        for (final String resource: this.resourceList) {
            try {
                this.resourceValidationMap.put(resource, getResponseCode(resource));
            } catch (final MalformedURLException e) {
            	logger.debug("Error in call method :: ValidateLinkedResource" + e.getMessage());
            } catch (final IOException e) {
            	logger.debug("Error in call method :: ValidateLinkedResource" + e.getMessage());
            }
        }
        return this.resourceValidationMap;
    }

    public static int getResponseCode(final String resource) throws IOException {

        final URL url = new URL(resource);

        if (url.openConnection() instanceof HttpURLConnection) {
            final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("HEAD"); // Retrieve only status code and headers not body of resposne.
            connection.setInstanceFollowRedirects(false);
            HttpURLConnection.setFollowRedirects(false); // Not to redirect the target url instead return actual status code (301 or 302)
            connection.connect();
            return connection.getResponseCode();
        }
        return -1; // return for FTP/SMTP ...protocols
    }

}
