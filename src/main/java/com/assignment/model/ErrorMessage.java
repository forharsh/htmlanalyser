package com.assignment.model;


public class ErrorMessage {

    private String detail;

    public  ErrorMessage(final String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return this.detail;
    }

    public void setDetail(final String detail) {
        this.detail = detail;
    }
}
