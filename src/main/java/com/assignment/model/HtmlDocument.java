package com.assignment.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HtmlDocument {
	
	private Map<String, Integer> headingTagMap = new HashMap<>();
    private Map<String, Integer> linkResourceValidationMap = new HashMap<>();
    private String uri;
    private String htmlVersion;
    private int internalLinksCount;
    private int externalLinksCount;
    private boolean hasLoginForm;
    private String pageTitle;
    
    
    public Map<String, Integer> getHeadingTagMap() {
		return headingTagMap;
	}
	public void setHeadingTagMap(final Map<String, Integer> headingTagMap) {
		this.headingTagMap = headingTagMap;
	}
	public Map<String, Integer> getLinkResourceValidationMap() {
		return linkResourceValidationMap;
	}
	public void setLinkResourceValidationMap(final Map<String, Integer> linkResourceValidationMap) {
		this.linkResourceValidationMap = linkResourceValidationMap;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(final String uri) {
		this.uri = uri;
	}
	public String getHtmlVersion() {
		return htmlVersion;
	}
	public void setHtmlVersion(final String htmlVersion) {
		this.htmlVersion = htmlVersion;
	}
	public int getInternalLinksCount() {
		return internalLinksCount;
	}
	public void setInternalLinksCount(final int internalLinksCount) {
		this.internalLinksCount = internalLinksCount;
	}
	public int getExternalLinksCount() {
		return externalLinksCount;
	}
	public void setExternalLinksCount(final int externalLinksCount) {
		this.externalLinksCount = externalLinksCount;
	}
	public boolean isHasLoginForm() {
		return hasLoginForm;
	}
	public void setHasLoginForm(final boolean hasLoginForm) {
		this.hasLoginForm = hasLoginForm;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(final String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	@Override
	public String toString() {
		return "HtmlDocument [uri=" + uri + ", htmlVersion=" + htmlVersion + ", internalLinksCount="
				+ internalLinksCount + ", externalLinksCount=" + externalLinksCount + ", hasLoginForm=" + hasLoginForm
				+ ", pageTitle=" + pageTitle + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(pageTitle,uri);
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final HtmlDocument htmlDocument = (HtmlDocument) obj;
		return Objects.equals(pageTitle, htmlDocument.pageTitle) && Objects.equals(uri, htmlDocument.uri);
	}
	
	
}
