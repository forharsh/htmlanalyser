package com.assignment.controller;

import java.io.IOException;
import java.util.List;

import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.assignment.model.ErrorMessage;
import com.assignment.model.HtmlDocument;
import com.assignment.util.HtmlParserUtil;

@Controller
public class HtmlParserController {

    private static final String DESTINATION = "index";
    private static final Logger logger = LoggerFactory.getLogger(HtmlParserController.class);

    @Autowired
    private UrlValidator urlValidator;
    

    @RequestMapping(value="/parser", method = RequestMethod.GET)
    public String input(final Model model) {
        model.addAttribute("htmlDocument", null);
        model.addAttribute("errorMsg", new ErrorMessage(""));
        return DESTINATION;
    }

    @RequestMapping(value="/parser", method = RequestMethod.POST)
    public String onStart(@ModelAttribute final HtmlDocument htmlDocument, final Model model) {
        final ErrorMessage error = new ErrorMessage("");
        final boolean isValidUrl = urlValidator.isValid(htmlDocument.getUri());
        if (!isValidUrl) {
            error.setDetail("Url is invalid. Please enter a valid url begin with http(s).");
            model.addAttribute("errorMsg", error);
            model.addAttribute("htmlDocument", null);
            logger.debug("--Invalid url in request--" + htmlDocument.getUri());
            return DESTINATION;
        }
        setPageInformation(htmlDocument, model, error);
        return DESTINATION;
    }

    private void setPageInformation(@ModelAttribute final HtmlDocument htmlDocument, final Model model, final ErrorMessage error) {
        try {
            final Document doc = Jsoup.connect(htmlDocument.getUri()).get();
            htmlDocument.setHtmlVersion(HtmlParserUtil.getHtmlDocType(doc));
            htmlDocument.setPageTitle(HtmlParserUtil.getPageTitle(doc));
            final List<String> hyperLinksCollection = HtmlParserUtil.getHyperLinksCollection(doc);
            final int[] linksCount = HtmlParserUtil.getHyperLinkCount(doc, hyperLinksCollection);
            htmlDocument.setInternalLinksCount(linksCount[0]);
            htmlDocument.setExternalLinksCount(linksCount[1]);
            htmlDocument.setHasLoginForm(HtmlParserUtil.hasLoginForm(doc));
            htmlDocument.setHeadingTagMap(HtmlParserUtil.getGroupedHeadingCount(doc));
            htmlDocument.setLinkResourceValidationMap(ResourceValidator.resourceValidator(hyperLinksCollection));

        } catch (final IOException e) {
        	 logger.debug("--Error in method setPageInformation::HtmlParserController--" + e.getMessage());
        }

        model.addAttribute("errorMsg", error);
        model.addAttribute("htmlDocument", htmlDocument);
    }
}
