package com.assignment.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.assignment.thread.ValidateLinkedResource;
import com.google.common.collect.Lists;

/**
 * Validate all input/external links with there status code 
 * by creating a pool of thread using ExecutorService.
 * @author harshvardhan
 *
 */
public class ResourceValidator {

	private static final Logger logger = LoggerFactory.getLogger(ResourceValidator.class);
	
    public static Map<String, Integer> resourceValidator(final List<String> resourceList){
        final Map<String, Integer> resourceValidationMap = new ConcurrentHashMap<>();

        //Decide the size of thread pool for optimal performance it should be no. of processors + 1;
        final int numOfThread = Runtime.getRuntime().availableProcessors() + 1; 
        
        logger.debug("Pool Created with active threads {}" , numOfThread);
        
        final ExecutorService threadPool = Executors.newFixedThreadPool(numOfThread);

        //Create a list to hold the Future object
        final List<FutureTask<Map<String, Integer>>> futureTaskList = new ArrayList<>();

        //partition the input list into small segments based on available processors.
        final List<List<String>> subSets = Lists.partition(resourceList, numOfThread);

        // java8 foreach feature to iterated the list and create the Future object.
        subSets.forEach(subList -> {
            final ValidateLinkedResource task = new ValidateLinkedResource(subList);
            final FutureTask<Map<String, Integer>> futureTask = new FutureTask<>(task);
            threadPool.submit(futureTask);
            futureTaskList.add(futureTask);
        });

        // java8 foreach feature to collect result from future object.
        futureTaskList.forEach(mapFutureTask -> {
            try {
                resourceValidationMap.putAll(mapFutureTask.get());
            } catch (final InterruptedException | ExecutionException e) {
               logger.debug("Error in Resource Validator :: resourceValidator " + e.getMessage());
            }
        });

        // shutdown all threads to become inactive.
        threadPool.shutdown();
        return resourceValidationMap;
    }
}
