package com.assignment.util;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * Parser Utility which helps to extract data from HTML page
 * ie. links, css classes, input elements etc. 
 * @author harshvardhan
 *
 */
public final class HtmlParserUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(HtmlParserUtil.class);

    private static final String H1 = "h1";
    private static final String H2 = "h2";
    private static final String H3 = "h3";
    private static final String H4 = "h4";
    private static final String H5 = "h5";
    private static final String H6 = "h6";
    
 // Private constructor to prevent instantiation
    private HtmlParserUtil() {
        throw new UnsupportedOperationException();
    }



    /**
     * Extract HTML title
     *
     * @param doc
     * @return
     */
    public static String getPageTitle(final Document doc) {
        return Optional.ofNullable(doc.title()).orElse("Title not available.");
    }

    /**
     * Extract HTML DOCTYPE (version)
     *
     * @param doc
     * @return
     */
    public static String getHtmlDocType(final Document doc) {

        String docType = "";
        final List<Node> nods = doc.childNodes();
        for (final Node node : nods) {
            if (node instanceof DocumentType) {
                final DocumentType documentType = (DocumentType) node;
                docType = documentType.toString();
                break;
            }
        }
        return docType;
    }

    /**
     * Number of headings grouped by heading level
     *
     * @param doc
     * @return
     */
    public static Map<String, Integer> getGroupedHeadingCount(final Document doc) {

        final Map<String, Integer> hTagsMap = new HashMap<>();

        final Elements hTags = doc.select("h1, h2, h3, h4, h5, h6");
        hTagsMap.put(H1, hTags.select(H1).size());
        hTagsMap.put(H2, hTags.select(H2).size());
        hTagsMap.put(H3, hTags.select(H3).size());
        hTagsMap.put(H4, hTags.select(H4).size());
        hTagsMap.put(H5, hTags.select(H5).size());
        hTagsMap.put(H6, hTags.select(H6).size());

        return hTagsMap;
    }

    /**
     * Get all hyperlinks from the html document
     * @param doc
     * @return List
     */
    public static List<String> getHyperLinksCollection(final Document doc) {

        final List<String> hyperLinkList = new ArrayList<>();
        final Elements links = doc.select("a[href]");

        for (final Element link : links) {
            final String href = link.attr("abs:href");

            if (!StringUtil.isBlank(href)) {
                hyperLinkList.add(href);
            }
        }
        return hyperLinkList;

    }

  /**
   * Get Hyperlink count internal or external links.
   * @param doc
   * @param hyperlinkCollection
   * @return int[]
   */
    public static int[] getHyperLinkCount(final Document doc, final List<String> hyperlinkCollection) {

        /**
         * use array to store the number of internal and external links
         * numOfLinks[0]: number of internal links
         * numOfLinks[1]: number of external links
         */
        final int[] numOfLinks = new int[2];
        int internalLinks = 0;
        int externalLinks = 0;

        try {
            final URL aURL = new URL(doc.baseUri());
            final String domain = aURL.getHost();

            for (final String link: hyperlinkCollection) {
                if (link.contains(domain)) {
                    internalLinks++;
                } else {
                    externalLinks++;
                }
            }
            numOfLinks[0] = internalLinks;
            numOfLinks[1] = externalLinks;

        } catch (final MalformedURLException e) {
        	logger.debug("Error in getHyperLinkCount :: getHyperLinkCount {}",e.getMessage());
        }
        return numOfLinks;
    }

   /**
    * Check if html page contains login form returns true 
    * if html document contains input type password element 
    * false otherwise.
    * @param doc
    * @returt
    */
    public static boolean hasLoginForm(final Document doc) {

        boolean hasLoginForm = false;
        final Elements inputs = doc.getElementsByTag("input");

        for (final Element element : inputs) {
            final String password = element.attr("type");
            if (!StringUtils.isEmpty(password) && "password".equalsIgnoreCase(password)) {
                hasLoginForm = true;
                break;
            }
        }
        return hasLoginForm;
    }
}
