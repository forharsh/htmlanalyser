package com.assignment.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
/**
 * Aspect to Log all request to methods as per defined pointcut.
 * @author harshvardhan
 *
 */
@Aspect
@Component
public class LoggerAspect {
	
	@Pointcut(value = "execution(* com.assignment.controller.*.*(..))")
	public void allMethodsPointcut(){}
	
	@Before("allMethodsPointcut()")
	public void allServiceMethodsAdvice(final JoinPoint joinPoint) {
		// Log every method in controller package.
		LoggerFactory.getLogger(joinPoint.getTarget().getClass()).info("{} method called with arguments {}",
				joinPoint.getSignature().getName(), joinPoint.getArgs());
	}

}
