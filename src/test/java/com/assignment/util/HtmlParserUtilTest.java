package com.assignment.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import com.assignment.util.HtmlParserUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class HtmlParserUtilTest {

	private Document documentNoLogin;

	@Before
	public void setup() {
		final ClassLoader classLoader = getClass().getClassLoader();
		final File inputNoLogin = new File(classLoader.getResource("test.html").getFile());
		try {
			this.documentNoLogin = Jsoup.parse(inputNoLogin, "UTF-8",
					"https://jsoup.org/cookbook/input/load-document-from-file");
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetTitle() {
		assertEquals(HtmlParserUtil.getPageTitle(this.documentNoLogin),
				"Load a Document from a File: jsoup Java HTML parser");
	}

	@Test
	public void testGetHtmlDocType() {
		assertEquals(HtmlParserUtil.getHtmlDocType(this.documentNoLogin), "<!doctype html>");
	}

	@Test
	public void testGetNumOfHeadingsByGroup() {
		assertEquals(HtmlParserUtil.getGroupedHeadingCount(this.documentNoLogin).toString(),
				"{h1=1, h2=4, h3=5, h4=1, h5=0, h6=0}");
	}

	@Test
	public void testGetHyperLinksCollection() {
		assertEquals(HtmlParserUtil.getHyperLinksCollection(this.documentNoLogin).size(), 29);
	}

	@Test
	public void testGetNumOfHyperMediaLink_Internal() {
		final List<String> links = HtmlParserUtil.getHyperLinksCollection(this.documentNoLogin);
		assertEquals(HtmlParserUtil.getHyperLinkCount(this.documentNoLogin, links)[0], 28);
	}
	
	@Test
	public void testGetNumOfHyperMediaLink_External() {
		final List<String> links = HtmlParserUtil.getHyperLinksCollection(this.documentNoLogin);
		assertEquals(HtmlParserUtil.getHyperLinkCount(this.documentNoLogin, links)[1], 1);
	}

	@Test
	public void testHasLoginFalse() {
		assertEquals("failure - did not match", HtmlParserUtil.hasLoginForm(this.documentNoLogin), false);
	}
}
