package com.assignment.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.assignment.model.HtmlDocument;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HtmlParserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getParseUrlTest() throws Exception {
		this.mockMvc.perform(get("/parser")).andExpect(status().isOk()).andReturn();
	}

	@Test
	public void postParseUrlTest() throws Exception {
		final HtmlDocument htmlDocument = new HtmlDocument();
		htmlDocument.setUri("http://www.google.com");
		this.mockMvc.perform(post("/parser").content(asJsonString(htmlDocument)))
				.andExpect(status().isOk()).andReturn();
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        return mapper.writeValueAsString(obj);
	    } catch (final Exception e) {
	        throw new RuntimeException(e);
	    }
	}

}
