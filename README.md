# Technical challenge

The challenge is to build a web application that allows user to conduct some analysis of an HTML web document.


### Tech Stack
The tech stack I choose to use are list as follow:
* Java 8
* Library
  * Jsoup (HTML Parser)
  * Guava (Util library)
  * Apache commons-validator (Util library)
* Spring-boot (Application)
* Bootstrap (CSS)

### Run the Application

You can package and run the backend application by executing the following command:

```
#!bash
mvn package spring-boot:run
```

This will start an instance running on the default port `8080`.

You can also build a single executable WAR file that contains all the necessary dependencies, classes, and resources, and run that.
The JAR file will be create by executing the command `mvn package`, and the WAR file will be located under target directory.

Then you can run the WAR file:
```
java -jar target/html-parser-1.0-SNAPSHOT.war
```

After successfully running the application, you can now open your browser with the following url:
 ```
 http://localhost:8080/parser
 ```

 Now you can start analyzing the url you want. For example: `http://www.google.com`.

### Application Design

##### Identify Login form
To check whether a login form is exist or not in the web document. Utility is checking if that web page contains 
 `type=password` , then it assumes there is a login form in this web document.  

##### Resource Validation
A web document can contain number of links. 
Performance matter a lot if we want to validate these resources.
  
We used a `ExecutorService.newFixedThreadPool` as a thread-pool to improve the performance. 
Thread size is set to 9 (`#Available Processor +1` is optimal on average).
      
Each thread will take a subset of the collected hyperlink, and check if the resource are reachable.
The way to know this is to check the HttpResponse Code.

Consider the effect of redirection, I simply set the connection object not to allow redirection. 

```
HttpURLConnection.setFollowRedirects(false);
```
If the hyperlink does not go through HTTP protocol(Example: FTP, SMTP), the response code will return `-1`.


##### Loggin Aspect

In order to log every request in controller. I have created a logger aspect with before advice that will log all methods and logs method signature and arguments.

  

